jQuery(window).resize(function(){
    slider_init();
});

$(function() {
    slider_init();
});

function slider_init(){

    var vis = 6;

    if(jQuery(window).outerWidth() < 1200){
        vis = 4;
    }

    if(jQuery(window).outerWidth() < 992){
        vis = 3;
    }

    if(jQuery(window).outerWidth() < 768){
        vis = 1;
    }

    if (jQuery('.klients-slider').length){
        jQuery('.klients-slider').jCarouselLite({
            btnNext: '.klients-next',
            btnPrev: '.klients-prev',
            visible: vis
        });
    }
}

jQuery(document).ready(function(){
    jQuery('.faq-item .answer').hide();
    jQuery('.question-form form').hide();
});

jQuery('.question span').click(function() {
    jQuery(this).parent().toggleClass('question-open');
    jQuery(this).parent().parent().find('.answer').toggle(300);
});

jQuery('.question-form .question-btn').click(function(){
    jQuery('.question-form form').show(300);
    jQuery('.question-form .question-btn').toggle(200);
});